"""
Defines datatypes used to represent sections of `fontite.toml` configuration
files, as well as logic for parsing them from unstructured dictionaries.
"""

import collections
from enum import Enum
from dataclasses import dataclass
import re
import sys
from typing import Optional, Union

class SupportedFontFormat(Enum):
    WOFF = 'woff'
    WOFF2 = 'woff2'
    TTF = 'ttf'
    OTF = 'otf'
    EOT = 'eot'
    SVG = 'svg'

    def css_value(self):
        """
        Corresponding CSS `format(...)` string for a `@font-face` declaration.
        """
        return {
            SupportedFontFormat.WOFF: 'woff',
            SupportedFontFormat.WOFF2: 'woff2',
            SupportedFontFormat.TTF: 'truetype',
            SupportedFontFormat.OTF: 'opentype',
            SupportedFontFormat.EOT: 'embedded-opentype',
            SupportedFontFormat.SVG: 'svg',
        }[self]

class HashAlgorithm(Enum):
    '''
    Hash algorithms used for cachebusting.

    Please submit an issue or MR upstream if you're interested in using a
    different algorithm!
    '''
    SHA256 = 'sha256'

@dataclass
class SourceFontIcon:
    """
    Represents an element of [sources.fonts.*.icons].
    """
    id: Union[str, int]
    rename: Optional[str]

    @staticmethod
    def from_config(config: Union[str, int, dict]) -> Union['SourceFontIcon', list[str]]:
        if isinstance(config, str):
            return SourceFontIcon(config, None)

        errors = []

        codepoint_no_rename_err = 'codepoint identifier for entries of [sources.fonts.*.icons.*] is only supported with a rename property'

        if isinstance(config, int):
            errors.append(codepoint_no_rename_err)
            return errors

        try:
            id = config['id']
        except KeyError:
            errors.append('[sources.fonts.*.icons.*.id] is required for long-form icon ids')

        try:
            rename = config['rename']
        except KeyError:
            rename = None

        if isinstance(id, int) and rename is None:
            errors.append(codepoint_no_rename_err)

        if rename is not None and not isinstance(rename, str):
            errors.append('[sources.fonts.*.icons.*.rename] must be a string')

        if len(errors) > 0:
            return errors

        return SourceFontIcon(id, rename)

@dataclass
class SourceFont:
    """
    Represents an element of [sources.fonts.*].
    """
    name: str
    path: str
    icons: list[SourceFontIcon]

    @staticmethod
    def from_config(name: str, config: dict) -> Union['SourceFont', list[str]]:
        errors = []

        try:
            path = config['path']
        except KeyError:
            errors.append('[sources.fonts.*.path] is required')

        try:
            icons = [SourceFontIcon.from_config(icon) for icon in config['icons']]
        except KeyError:
            icons = []

        for icon in icons:
            if isinstance(icon, list):
                errors.extend(icon)

        if len(errors) > 0:
            return errors

        return SourceFont(name, path, icons)

@dataclass
class SourceGlyph:
    """
    Represents an element of [sources.glyphs.*].
    """
    name: str
    path: str

    @staticmethod
    def from_config(name: str, config: dict) -> Union['SourceGlyph', list[str]]:
        errors = []

        try:
            path = config['path']
        except KeyError:
            errors.append('[sources.glyphs.*.path] is required')

        if len(errors) > 0:
            return errors

        return SourceGlyph(name, path)

@dataclass
class BaseSettingsFont:
    """
    Represents [sources.base_settings_font].
    """
    path: str

    @staticmethod
    def from_config(config: dict) -> Union['BaseSettingsFont', list[str]]:
        errors = []

        try:
            path = config['path']
        except KeyError:
            errors.append('[sources.base_settings_font.path] is required')

        if len(errors) > 0:
            return errors

        return BaseSettingsFont(path)

@dataclass
class Sources:
    """
    Represents [sources].
    """
    base_settings_font: Optional[BaseSettingsFont]
    fonts: list[SourceFont]
    glyphs: list[SourceGlyph]

    @staticmethod
    def from_config(config: dict) -> Union['Sources', list[str]]:
        errors = []

        try:
            base_settings_font = BaseSettingsFont.from_config(config['base_settings_font'])
            if isinstance(base_settings_font, list):
                errors.extend(base_settings_font)
        except KeyError:
            base_settings_font = None

        try:
            font_sources = [SourceFont.from_config(name, config['fonts'][name]) for name in config['fonts']]
        except KeyError:
            font_sources = []

        for font_source in font_sources:
            if isinstance(font_source, list):
                errors.extend(font_source)

        try:
            glyph_sources = [SourceGlyph.from_config(name, config['glyphs'][name]) for name in config['glyphs']]
        except KeyError:
            glyph_sources = []

        for glyph_source in glyph_sources:
            if isinstance(glyph_source, list):
                errors.extend(glyph_source)

        if len(font_sources) + len(glyph_sources) == 0:
            errors.append('At least one font or glyph source must be specified')

        if len(errors) > 0:
            return errors

        assert not isinstance(base_settings_font, list)

        return Sources(base_settings_font, font_sources, glyph_sources)

@dataclass
class FontsOutput:
    """
    Represents [outputs.fonts].
    """
    formats: list[SupportedFontFormat]
    filename: str
    dir: str

    @staticmethod
    def from_config(config: dict) -> Union['FontsOutput', list[str]]:
        errors = []

        try:
            formats_raw = config['formats']
        except KeyError:
            errors.append('[outputs.fonts.formats] is required')
            formats_raw = []

        if not isinstance(formats_raw, list) or len(formats_raw) < 1 or not any([isinstance(e, str) for e in formats_raw]):
            errors.append('[outputs.fonts.formats] must be a list of strings with at least one element')

        formats = []
        for format in formats_raw:
            try:
                formats.append(SupportedFontFormat(format))
            except ValueError:
                errors.append(f'"{format}" is not a supported format. Try one of ' + str([format.value for format in SupportedFontFormat]))

        try:
            filename = config['filename']
        except KeyError:
            errors.append('[outputs.fonts.filename] is required')

        try:
            dir = config['dir']
        except KeyError:
            errors.append('[outputs.fonts.dir] is required')

        if len(errors) > 0:
            return errors

        return FontsOutput(formats, filename, dir)

@dataclass
class CssCachebust:
    '''
    Represents [outputs.css.cachebust].
    '''
    query: str
    algorithm: HashAlgorithm
    truncate: Optional[int]

    @staticmethod
    def from_config(config: dict) -> Union['CssCachebust', list[str]]:
        errors = []

        try:
            query = config['query']
        except KeyError:
            errors.append('[outputs.css.cachebust.query] is required')

        try:
            algorithm_raw = config['algorithm']
            try:
                algorithm = HashAlgorithm(algorithm_raw)
            except ValueError:
                errors.append(f'"{algorithm_raw}" is not a supported hash algorithm. Try one of ' + str([algo.value for algo in HashAlgorithm]))
        except KeyError:
            errors.append('[outputs.css.cachebust.algorithm] is required')

        try:
            truncate = config['truncate']
        except KeyError:
            truncate = None

        if len(errors) > 0:
            return errors

        return CssCachebust(query, algorithm, truncate)

@dataclass
class CssOutput:
    """
    Represents [outputs.css].
    """
    path: str
    font_url: str
    icon_class: str
    icon_class_prefix: str
    cachebust: Optional[CssCachebust]

    @staticmethod
    def from_config(config: dict) -> Union['CssOutput', list[str]]:
        errors = []

        try:
            path = config['path']
        except KeyError:
            errors.append('[outputs.css.path] is required')

        try:
            font_url = config['font_url']
        except KeyError:
            errors.append('[outputs.css.font_url] is required')

        try:
            icon_class = config['icon_class']
        except KeyError:
            errors.append('[outputs.css.icon_class] is required')

        try:
            icon_class_prefix = config['icon_class_prefix']
        except KeyError:
            errors.append('[outputs.css.icon_class_prefix] is required')

        try:
            cachebust = CssCachebust.from_config(config['cachebust'])
        except KeyError:
            cachebust = None

        if isinstance(cachebust, list):
            errors.extend(cachebust)

        if len(errors) > 0:
            return errors

        return CssOutput(path, font_url, icon_class, icon_class_prefix, cachebust)

@dataclass
class Outputs:
    """
    Represents [outputs].
    """
    fonts: FontsOutput
    css: Optional[CssOutput]

    @staticmethod
    def from_config(config: dict) -> Union['Outputs', list[str]]:
        errors = []

        try:
            fonts = FontsOutput.from_config(config['fonts'])
            if isinstance(fonts, list):
                errors.extend(fonts)
        except KeyError:
            errors.append('[outputs.fonts] is required')

        try:
            css = CssOutput.from_config(config['css'])
            if isinstance(css, list):
                errors.extend(css)
        except KeyError:
            css = None

        if len(errors) > 0:
            return errors

        assert not isinstance(fonts, list)
        assert not isinstance(css, list)

        return Outputs(fonts, css)

@dataclass
class ParsedConfig:
    """
    Represents the entire `fontite.toml` config file.
    """
    sources: Sources
    outputs: Outputs

    @staticmethod
    def from_config(config: dict) -> Union['ParsedConfig', list[str]]:
        errors = []

        try:
            sources = Sources.from_config(config['sources'])
            if isinstance(sources, list):
                errors.extend(sources)
        except KeyError:
            errors.append('[sources] is required')

        try:
            outputs = Outputs.from_config(config['outputs'])
            if isinstance(outputs, list):
                errors.extend(outputs)
        except KeyError:
            errors.append('[outputs] is required')

        if len(errors) > 0:
            return errors

        assert not isinstance(sources, list)
        assert not isinstance(outputs, list)

        return ParsedConfig(sources, outputs)

def all_icon_names(sources: Sources) -> list[str]:
    names = []

    for font_source in sources.fonts:
        for icon in font_source.icons:
            if icon.rename is not None:
                icon_name = icon.rename
            else:
                assert not isinstance(icon.id, int)
                icon_name = icon.id
            names.append(icon_name)

    for glyph_source in sources.glyphs:
        names.append(glyph_source.name)

    return names

def parse_config(config: dict) -> ParsedConfig:
    errors = []

    parsed = ParsedConfig.from_config(config)

    if isinstance(parsed, list):
        errors.extend(parsed)

    if len(errors) > 0:
        for error in errors:
            print('Error: ' + error)
        sys.exit(-1)

    assert not isinstance(parsed, list)

    icon_names = all_icon_names(parsed.sources)

    duplicates = [item for item, count in collections.Counter(icon_names).items() if count > 1]
    if len(duplicates) > 0:
        for ident in duplicates:
            print(f'Error: Icon name "{ident}" appears multiple times in the configuration. Remove or rename duplicates.')
        sys.exit(-1)

    if parsed.outputs.css is not None:
        css_safe_identifier = re.compile('^[a-zA-Z0-9\-_]+$')
        invalid_identifiers = [name for name in icon_names if not css_safe_identifier.match(name)]
        if len(invalid_identifiers) > 0:
            for ident in invalid_identifiers:
                print(f'Error: Icon name "{ident}" may not be a valid in a CSS class name. Use a `rename` property to ensure it only has alphanumeric characters, dashes, and underscores.')
            sys.exit(-1)

    return parsed
