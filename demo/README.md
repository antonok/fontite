# fontite demo

This directory contains sources for a `fontite` demo page.
You can use this to verify that `fontite` is working correctly, or to see an example of how to integrate it into a real page.

## Setup

Once all dependencies are installed, simply run `setup.sh`.

## Viewing

The website will be built in the `static` directory.
You can serve it with any static webserver; for example:

```sh
cd static
python3 -m http.server
```
