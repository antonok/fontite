#!/usr/bin/sh

if [ `basename $PWD` != "demo" ]; then
    echo "Please run this script from the 'fontite/demo' directory."
    exit 1
fi

mkdir ../input
cd ../input

# Download and unpack sources

if [ ! -f "fa-brands-400.woff2" ]; then
    wget "https://use.fontawesome.com/releases/v5.11.0/webfonts/fa-brands-400.woff2"
fi

if [ ! -f "openwebicons.woff2" ]; then
    wget "https://github.com/pfefferle/openwebicons/archive/refs/tags/1.6.3.zip" --output-document="openwebicons.zip"
    unzip -j "openwebicons.zip" "openwebicons-1.6.3/font/openwebicons.woff2"
    rm "openwebicons.zip"
fi

if [ ! -f "fa-solid-900.woff2" ]; then
    wget "https://use.fontawesome.com/releases/v5.11.0/webfonts/fa-solid-900.woff2"
fi

if [ ! -f "forkawesome-webfont.woff2" ]; then
    wget "https://github.com/ForkAwesome/Fork-Awesome/archive/1.2.0.zip" --output-document="forkawesome.zip"
    unzip -j "forkawesome.zip" "Fork-Awesome-1.2.0/fonts/forkawesome-webfont.woff2"
    rm "forkawesome.zip"
fi

if [ ! -f "fontite.svg" ]; then
    cp ../fontite.svg .
fi

if [ ! -f "iconify2.svg" ]; then
    wget "https://api.iconify.design/line-md/iconify2.svg"
fi

if [ ! -f "ipfs.svg" ]; then
    wget "https://api.iconify.design/simple-icons/ipfs.svg"
fi

# Create the static site under ./static/

cd ../demo

mkdir -p static/fonts
cp demopage.html static/index.html

cp ../fontite.svg static/fontite.svg

../fontite

ORIGINALSIZE=0;
for size in $(stat --format='%s' ../input/*); do
    ORIGINALSIZE=$(( $ORIGINALSIZE + $size ));
done;

FONTITESIZE=0;
for size in $(stat --format='%s' static/fontite.css static/fonts/all-icons.woff2); do
    FONTITESIZE=$(( $FONTITESIZE + $size ));
done;

echo "Original source files are $ORIGINALSIZE bytes in total."
echo "Fontite WOFF2 + CSS outputs are just $FONTITESIZE bytes in total."

echo "You can now serve the demo page from the 'static' directory, e.g. using 'python3 -m http.server'."
